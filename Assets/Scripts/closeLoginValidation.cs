﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Linq;
using System.Text;
using UnityEngine.Networking;
using UnityEngine.EventSystems;
using GoogleMobileAds.Api;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;
using System;

public class closeLoginValidation : MonoBehaviour
{
    public bool isDisconnectedPage;
    IEnumerator checkInternetConnection(Action<bool> action)
    {
        WWW www = new WWW("http://google.com");
        yield return www;
        if (www.error != null)
        {
            action(false);
        }
        else
        {
            action(true);
        }
    }

    void Start()
    {
        Screen.SetResolution(800, 480, true);
        StartCoroutine(checkInternetConnection((isConnected) => {
            if (!isConnected)
            {
                if (isDisconnectedPage == false)
                {
                    SceneManager.LoadScene(2);
                }
            }
        }));
    }

    void OnApplicationQuit()
    {
        Login Udata = new Login();
        Debug.Log(PlayerPrefs.GetString("bossgame_nickname"));
        StartCoroutine(Udata.postQuit(PlayerPrefs.GetString("bossgame_nickname")));
        StartCoroutine(Udata.postPlayTimePlayingGame(PlayerPrefs.GetString("bossgame_iduser"), "20", PlayerPrefs.GetString("FirstPlay"), System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
        Debug.Log(PlayerPrefs.GetString("FirstPlay") + " " + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        Application.Quit();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Login Udata = new Login();
            Debug.Log(PlayerPrefs.GetString("bossgame_nickname"));
            StartCoroutine(Udata.postQuit(PlayerPrefs.GetString("bossgame_nickname")));
            StartCoroutine(Udata.postPlayTimePlayingGame(PlayerPrefs.GetString("bossgame_iduser"), "20", PlayerPrefs.GetString("FirstPlay"), System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
            Debug.Log(PlayerPrefs.GetString("FirstPlay") + " " + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            Application.Quit();
        }
    }


}
