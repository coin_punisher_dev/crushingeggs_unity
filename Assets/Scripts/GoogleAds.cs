﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using GoogleMobileAds.Api;

public class GoogleAds : MonoBehaviour
{
    public BannerView bannerView;

    public void RequestBanner()
    {
        #if UNITY_ANDROID
			//Testing
            //string adUnitId = "ca-app-pub-3940256099942544/6300978111";
			//Production
            string adUnitId = "ca-app-pub-3028353481482626/6208926109";
        #elif UNITY_IPHONE
            string adUnitId = "ca-app-pub-3940256099942544/2934735716";
        #else
            string adUnitId = "unexpected_platform";
        #endif

        // Create a 320x50 banner at the Bottom of the screen.
        AdSize adSize = new AdSize(320, 50);
        this.bannerView = new BannerView(adUnitId, adSize, AdPosition.Bottom);

        // Called when an ad request has successfully loaded.
        this.bannerView.OnAdLoaded += this.HandleOnAdLoaded;
        // Called when an ad request failed to load.
        this.bannerView.OnAdFailedToLoad += this.HandleOnAdFailedToLoad;
        // Called when an ad is clicked.
        this.bannerView.OnAdOpening += this.HandleOnAdOpened;
        // Called when the user returned from the app after an ad click.
        this.bannerView.OnAdClosed += this.HandleOnAdClosed;
        // Called when the ad click caused the user to leave the application.
        this.bannerView.OnAdLeavingApplication += this.HandleOnAdLeavingApplication;

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();

        // Load the banner with the request.
        this.bannerView.LoadAd(request);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }
	
	public void startBanner(){
		#if UNITY_ANDROID
			//Testing
            //string appId = "ca-app-pub-3940256099942544~3347511713";
			//Production
            string appId = "ca-app-pub-3028353481482626~2461252782";
        #elif UNITY_IPHONE
            string appId = "ca-app-pub-3940256099942544~1458002511";
        #else
            string appId = "unexpected_platform";
        #endif

        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(appId);
		this.RequestBanner();
	}
	
	public void HandleOnAdLoaded(object sender, EventArgs args)
    {
        Login ms = new Login();
        StartCoroutine(ms.PostRunningAds(PlayerPrefs.GetString("bossgame_nickname"), "HandleOnAdLoaded", 1, 0.0));
        MonoBehaviour.print("HandleOnAdLoaded event received");
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print("HandleOnAdFailedToLoad event received with message: " + args.Message);
        Login ms = new Login();
        StartCoroutine(ms.PostRunningAds(PlayerPrefs.GetString("bossgame_nickname"), "HandleOnAdFailedToLoad", 2, 0.0));
    }

    public void HandleOnAdOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleOnAdOpened event received");
        Login ms = new Login();
        StartCoroutine(ms.PostRunningAds(PlayerPrefs.GetString("bossgame_nickname"), "HandleOnAdOpened", 3, 0.0));
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleOnAdClosed event received");
        Login ms = new Login();
        StartCoroutine(ms.PostRunningAds(PlayerPrefs.GetString("bossgame_nickname"), "HandleOnAdClosed", 5, 0.0));
    }

    public void HandleOnAdLeavingApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleOnAdLeavingApplication event received");

        Login ms = new Login();
        StartCoroutine(ms.PostRunningAds(PlayerPrefs.GetString("bossgame_nickname"), "HandleOnAdLeavingApplication", 7, 0.0));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
