using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreOnComplete : MonoBehaviour {

    public Text FinalScore;

    void Start()
    {
        FinalScore.text = globalVars.scoreTotal.ToString();
    }
}
