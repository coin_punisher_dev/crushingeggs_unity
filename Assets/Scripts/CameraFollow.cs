using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using SimpleJSON;
using UnityEngine.Advertisements;

public class CameraFollow : MonoBehaviour {

	public float smoothSpeed = 0.125f;
	public Vector3 offset;

    private GameObject gm;

    public bool ViewingScene = true;

    public bool ViewingEnemies = false;

    public GameObject[] targets;

    public int targetnumber = 1;

    public int ballnumber = 1;

    public bool IncreaseBallNumber = false;
	
	public GameObject FailedMenuGO;

    IEnumerator WaitingPopup()
    {
        //Print the time of when the function is first called.
        PlayerPrefs.SetString("statusAds", "0");
        Debug.Log("Started Coroutine at timestamp : " + Time.time);

        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(10);
        FailedMenuGO.SetActive(true);
        globalVars.stopPlaying = true;
        PlayerPrefs.SetString("statusAds", "1");

        //After we have waited 5 seconds print the time again.
        Debug.Log("Finished Coroutine at timestamp : " + Time.time);
    }

    private void Start()
    {
        Invoke("StopViewingScene", 3f);
        gm = GameObject.Find("GameMaster");
    }

    void FixedUpdate ()
	{
		Debug.Log(gm.GetComponent<GameMaster>().EnemiesAlive+" "+ targetnumber);
		if(gm.GetComponent<GameMaster>().EnemiesAlive >= 0 && targetnumber >= 4)
        {
            StartCoroutine(WaitingPopup());
        }
        else {
            if (targetnumber < 4)
            {
                if (ViewingScene == false)
                {
                    Vector3 desiredPosition = targets[targetnumber].transform.position;
                    desiredPosition.z = -10;
                    Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
                    transform.position = smoothedPosition;
                }

                if (targetnumber != 0 && ViewingEnemies == false && targets[targetnumber].GetComponent<Ball>().FirstHit == true)
                {
                    Invoke("ViewEnemies", 2f);
                }
            }
            
		}
		if (ViewingEnemies == true)
		{
			Invoke("ViewNextBall", 3f);
		}

	}

    public void ViewNextBall()
    {
        if (IncreaseBallNumber == true)
        {
			if(targetnumber >= 4){
				targetnumber--;
			}else{
				targetnumber += ballnumber;
				IncreaseBallNumber = false;
				ViewingEnemies = false;
			}
        }

    }

    public void ViewEnemies ()
    {
        if (IncreaseBallNumber == false)
        {
            targetnumber = 0;
            ViewingEnemies = true;
            ballnumber++;
            IncreaseBallNumber = true;
        }
    }

    public void StopViewingScene()
    {
        ViewingScene = false;
    }

}
