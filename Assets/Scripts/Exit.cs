using System;
using System.Collections;
using UnityEngine;

public class Exit : MonoBehaviour {

    public string CurrentPage;
    public AudioSource click;

    IEnumerator ExitGame()
    {
        yield return new WaitForSeconds(2);
        Application.Quit();
        Debug.Log("Quit Game");
    }

    // Use this for initialization
    void Start()
    {
        
    }
	
	// Update is called once per frame
	void Update () {
    }


    void OnMouseDown()
    {
        if (CurrentPage == "StartPage")
        {
            Login Udata = new Login();
            Debug.Log(PlayerPrefs.GetString("bossgame_nickname"));
            StartCoroutine(Udata.postQuit(PlayerPrefs.GetString("bossgame_nickname")));
            StartCoroutine(Udata.postPlayTimePlayingGame(PlayerPrefs.GetString("bossgame_iduser"), "20", PlayerPrefs.GetString("FirstPlay"), System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
            Debug.Log(PlayerPrefs.GetString("FirstPlay") + " " + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            Animator animatorExitGame = GetComponent<Animator>();
            animatorExitGame.SetInteger("exitOnIdlePlay", 1);
            StartCoroutine(ExitGame());
        }
        else
        {
            Login Udata = new Login();
            Debug.Log(PlayerPrefs.GetString("bossgame_nickname"));
            StartCoroutine(Udata.postQuit(PlayerPrefs.GetString("bossgame_nickname")));
            StartCoroutine(Udata.postPlayTimePlayingGame(PlayerPrefs.GetString("bossgame_iduser"), "20", PlayerPrefs.GetString("FirstPlay"), System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
            Debug.Log(PlayerPrefs.GetString("FirstPlay") + " " + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            Animator animatorExitGame = GetComponent<Animator>();
            animatorExitGame.SetInteger("exitOnIdlePlay", 1);
            StartCoroutine(ExitGame());
        }
        click.Play();
    }
}
