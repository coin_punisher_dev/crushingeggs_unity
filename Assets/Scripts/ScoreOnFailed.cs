using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreOnFailed : MonoBehaviour {

    public Text FinalScore;
    private GameObject gm;

    private void Start()
    {
        gm = GameObject.Find("GameMaster");
    }

    void Update()
    {
        FinalScore.text = gm.GetComponent<GameMaster>().score.ToString();
    }
}
