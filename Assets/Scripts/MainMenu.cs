using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using SimpleJSON;

public class MainMenu : MonoBehaviour {

    // Use this for initialization
    public string CurrentPage;
    public AudioSource click;

    IEnumerator postGetProfile2(string nickname)
    {
        Login lg = new Login();
        WWWForm form = new WWWForm();
        form.AddField("nickname", nickname);
        form.AddField("gameID", 20);

        UnityWebRequest www = UnityWebRequest.Post(lg.base_url + "cekNickName.php", form);
        yield return www.SendWebRequest();
        Debug.Log(www.downloadHandler.text);
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("Success Send API Profile Login");
            string a = www.downloadHandler.text;
            Debug.Log(a);
            var jsonObject = JSON.Parse(a);
            Debug.Log(jsonObject["value"]);
            if (jsonObject["value"] == 2)
            {
                PlayerPrefs.SetInt("bossgame_value", jsonObject["value"]);
            }
            else
            {

                if (jsonObject["value"] == 1)
                {
                    PlayerPrefs.SetString("bossgame_heart", jsonObject["heart"]);
                    PlayerPrefs.SetInt("bossgame_value", jsonObject["value"]);
                    Debug.Log("hasil nyawa : " + PlayerPrefs.GetString("bossgame_heart"));
                    //SceneManager.LoadScene(13);
                }
            }
        }
    }



    IEnumerator PlayToStart()
    {
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene(3);
    }
    IEnumerator GoLogin()
    {
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene(1);
    }
    void Start()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();

        PlayerPrefs.SetInt("klikPlay", 1);
    }
	
	// Update is called once per frame
	void Update () {
    }


    void OnMouseDown()
    {
        if (CurrentPage == "StartPage")
        {
            Animator animatorPlayInStart = GetComponent<Animator>();
            animatorPlayInStart.SetInteger("OnIdleStartPlay", 1);
            StartCoroutine(GoLogin());
        }
        else
        {
            if (PlayerPrefs.GetInt("klikPlay") == 1)
            {
                PlayerPrefs.SetInt("klikPlay", 0);
                Login lg = new Login();
                StartCoroutine(lg.postQuit(PlayerPrefs.GetString("bossgame_nickname")));
                StartCoroutine(postGetProfile2(PlayerPrefs.GetString("bossgame_nickname")));
                Animator animatorPlayagain = GetComponent<Animator>();
                animatorPlayagain.SetInteger("playagainOnIdleClicked", 1);
                StartCoroutine(PlayToStart());
            }
        }
        globalVars.stopPlaying = false;
        click.Play();
    }
}
